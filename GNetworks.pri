#-------------------------------------------------
#
# Fichier pri pour GNetworks
#
#-------------------------------------------------
QT += network
# Information programme à rajouter dans le fichier pri d'origine
LIB_NAME = GNetworks
LIB_VERSION = 1
LIB_ANDROID_VERSION = 1.0.1.1
LIB_PWD = $$PWD
# Complete informations Guitronic par default
include("$$PWD/../GlobalIncludeGuitronic.pri")
