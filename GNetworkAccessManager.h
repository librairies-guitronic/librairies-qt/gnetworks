#ifndef GNETWORKACCESSMANAGER_H
#define GNETWORKACCESSMANAGER_H

#include "gnetworks_global.h"
#include <QNetworkAccessManager>
#include <QNetworkReply>

#define	STRUCT_PROPERTY		QPair<QString, QVariant>
#define	LIST_PROPERTY		QList< QPair<QString, QVariant> >
#define	STRUCT_POST_QUERY	QPair<QString, QString>
#define	STRUCT_POST_TEXTE	QPair<QString, QString>
#define	STRUCT_POST_FILE	QPair<QString, QString>

class GNETWORKSSHARED_EXPORT GNetworkAccessManager : public QNetworkAccessManager
{
	Q_OBJECT

public:
	explicit GNetworkAccessManager(QObject *parent = Q_NULLPTR);

	/**
	 * Envois une requète GET pour recevoir du JSon
	 * @brief getJSon
	 * @param urlString
	 * @param listProps
	 */
	QNetworkReply *getJSon(const QString &urlString, const LIST_PROPERTY &listProps = LIST_PROPERTY());

	/**
	 * Envois une requète DELETE pour recevoir du JSon
	 * @brief deleteJSon
	 * @param urlString
	 * @param listProps
	 */
	QNetworkReply *deleteJSon(const QString &urlString, const LIST_PROPERTY &listProps = LIST_PROPERTY());

	/**
	 * Envois d'une requète en application/x-www-form-urlencoded
	 * @brief postQuery
	 * @param urlString
	 * @param listeQuery
	 * @param listProps
	 * @return
	 */
	QNetworkReply *postQuery(const QString &urlString, const QList< STRUCT_POST_QUERY > &listeQuery, const LIST_PROPERTY &listProps = LIST_PROPERTY());

	/**
	 * Envois de fichier post en multipart/form-data
	 * @brief postFile
	 * @param urlString
	 * @param name
	 * @param fileName
	 * @param listProps
	 * @return
	 */
	QNetworkReply *postFile(const QString &urlString, const QString &name, const QString &fileName, const LIST_PROPERTY &listProps = LIST_PROPERTY());

	/**
	 * Envois plusieurs éléments textes ou fichiers en QHttpMultiPart::FormDataType
	 * @brief postMultipart
	 * @param urlString
	 * @param listeTextes
	 * @param listeFichiers
	 * @param listProps
	 * @return
	 */
	QNetworkReply *postMultipart(const QString &urlString, const QList< STRUCT_POST_TEXTE > &listeTextes, const QList< STRUCT_POST_TEXTE > &listeFichiers, const LIST_PROPERTY &listProps = LIST_PROPERTY());

private Q_SLOTS:
	/**
	 * Envois post terminé
	 * @brief _networkFinished
	 */
	void    _networkFinished(QNetworkReply *reply);

	/**
	 * Pour debug
	 */
	/*void	_authenticationRequired(QNetworkReply *reply, QAuthenticator *authenticator);
	void	_preSharedKeyAuthenticationRequired(QNetworkReply *reply, QSslPreSharedKeyAuthenticator *authenticator);
	void	_encrypted(QNetworkReply *reply);
	void	_proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *authenticator);
	void	_sslErrors(QNetworkReply *reply, const QList<QSslError> &errors);*/

private:
	/**
	 * Assigne les propriétés à la reponse
	 * @brief _assignProperty
	 * @param response
	 * @param listProps
	 */
	void	_assignProperty(QNetworkReply *response, const LIST_PROPERTY &listProps = LIST_PROPERTY());

	/**
	 * Connecte les reponses pour les retours
	 * @brief _connectResponse
	 * @param response
	 */
	void	_connectResponse(QNetworkReply *response);

Q_SIGNALS:
	/**
	 * Progression de la requete
	 * @brief replyProgress
	 */
	void	replyProgress(qint64,qint64);

	/**
	 * Erreur de requète
	 * @brief replyError
	 * @param code, errors
	 */
	void    replyError(const QNetworkReply::NetworkError &code);
	void    replyError(const QList<QSslError> &errors);

	/**
	 * Récupération de la réponse à la requète
	 * @brief reponseDone
	 * @param reply, response
	 */
	void    reponseDone(QNetworkReply &);

	/**
	 * Signal qu'une requete est en cours
	 * @brief onBusy
	 * @param busy
	 */
	void	onBusy(bool busy);
};

#endif // GNETWORKACCESSMANAGER_H
