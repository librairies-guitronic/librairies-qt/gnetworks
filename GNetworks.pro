#-------------------------------------------------
#
# Project created by QtCreator 2018-12-25T15:48:29
#
#-------------------------------------------------
QT       += network
QT       -= gui

DEFINES += GNETWORKS_LIBRARY

# Version programme
TARGET = GNetworks
VERSION = 1.0.1.1
QMAKE_TARGET_PRODUCT = "Librairie Networks Guitronic pour Qt"$$QT_VERSION
QMAKE_TARGET_DESCRIPTION = "Permet l envois de requete POST facile. Prise en charge SSL"
# Complete informations Guitronic par default
include("$$PWD/../GlobalLibGuitronic.pro")

SOURCES += \
    GNetworkAccessManager.cpp

HEADERS += \
    gnetworks_global.h \
    GNetworkAccessManager.h
