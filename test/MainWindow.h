#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QFile>
#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QNetworkReply>
#include "GNetworkAccessManager.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = nullptr);
	~MainWindow();

private slots:
	void on_toolButton_released();

	/**
	 * Affiche une erreur si présente, arrete la progression
	 * @brief _networkFinished
	 * @param reply
	 */
	void    _networkFinished(QNetworkReply *reply);

	void	_actionProgress(qint64,qint64);

	void    _reponseDone(QNetworkReply &reply);

	/**
	 * Erreur de téléchargement
	 * @brief _error
	 * @param code
	 */
	void    _error(const QNetworkReply::NetworkError &code);
	void    _error(const QList<QSslError> &errors);

	void	on_toolButton_multi_released();

private:
	QByteArray _aadata;

	/**
	 * Acces manager pour le téléchargement
	 * @brief _networkManager
	 */
	GNetworkAccessManager *_networkManager;

	Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
