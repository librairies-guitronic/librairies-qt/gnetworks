#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <QDebug>
#include <QHttpMultiPart>

//#define	URL_TEST	"http://test.hm/upload.php"
#define	URL_TEST	"http://www.prog.guitronic.fr/upload.php"

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	_networkManager(new GNetworkAccessManager(this)),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	ui->lineEdit->setText(":/image/araignee");

	//Connection du networkManager
	connect(_networkManager, SIGNAL(finished(QNetworkReply*)), this, SLOT(_networkFinished(QNetworkReply*)));
	connect(_networkManager, SIGNAL(replyProgress(qint64,qint64)), this, SLOT(_actionProgress(qint64,qint64)));
	connect(_networkManager, SIGNAL(reponseDone(QNetworkReply &)), this, SLOT(_reponseDone(QNetworkReply &)));
	connect(_networkManager, SIGNAL(replyError(QNetworkReply::NetworkError)), this, SLOT(_error(QNetworkReply::NetworkError)) );
	connect(_networkManager, SIGNAL(replyError(QList<QSslError>)), this, SLOT(_error(QList<QSslError>)) );
}

MainWindow::~MainWindow()
{
	delete _networkManager;
	delete ui;
}

void MainWindow::on_toolButton_released()
{
	//Ajout du fichier
	QString fileName = ui->lineEdit->text();
	//E:/SITE_2.0/mvalerio/data/images/Macro/Araignees/araignee_crabe1.jpg
	//C:/Users/Guillaume/Desktop/3d-lg-2012-rio-carnival-(www.demolandia.net).m2ts

	qDebug() << "postFile" << fileName << URL_TEST
		<< _networkManager->postFile(URL_TEST, "upload", fileName);
}


void MainWindow::on_toolButton_multi_released()
{
	//Ajout du fichier
	QString fileName = ui->lineEdit->text();
	//E:/SITE_2.0/mvalerio/data/images/Macro/Araignees/araignee_crabe1.jpg
	//C:/Users/Guillaume/Desktop/3d-lg-2012-rio-carnival-(www.demolandia.net).m2ts

	qDebug() << "postMultipart" << fileName << URL_TEST;
	_networkManager->postMultipart(URL_TEST,
								   QList< STRUCT_POST_TEXTE >() << STRUCT_POST_TEXTE("text", "Test de texte"),
								   QList< STRUCT_POST_FILE >() << STRUCT_POST_FILE("upload", fileName));
}



/**
 * Affiche une erreur si présente, arrete la progression
 * @brief _networkFinished
 * @param reply
 */
void MainWindow::_networkFinished(QNetworkReply *reply)
{
	qDebug() << "_networkFinished" << reply->bytesAvailable()
			 << reply->errorString()
			 << reply->error()
			 << reply->rawHeader("Location")
			 << reply->attribute(QNetworkRequest::HttpStatusCodeAttribute);
}


void MainWindow::_actionProgress(qint64 int1, qint64 int2)
{
	qDebug() << "_actionProgress" << int1 << int2;
}

void MainWindow::_reponseDone(QNetworkReply &reply)
{
	qDebug() << "bytesAvailable" << reply.bytesAvailable();
	qDebug() << "ContentTypeHeader" << reply.header(QNetworkRequest::ContentTypeHeader).toString();
	qDebug() << "HttpStatusCodeAttribute" << reply.attribute(QNetworkRequest::HttpStatusCodeAttribute).toInt();
	qDebug() << "HttpReasonPhraseAttribute" << reply.attribute(QNetworkRequest::HttpReasonPhraseAttribute).toString();
	qDebug() << "SourceIsFromCacheAttribute" << reply.attribute(QNetworkRequest::SourceIsFromCacheAttribute).toBool();
	qDebug() << "DoNotBufferUploadDataAttribute" << reply.attribute(QNetworkRequest::DoNotBufferUploadDataAttribute).toBool();
	if( reply.error() != QNetworkReply::NoError )
	{
		qDebug() << "erreur" << reply.errorString();
	}

	ui->textEdit->setHtml( reply.readAll() );
}




/**
 * Erreur de téléchargement
 * @brief _error
 * @param code
 */
void    MainWindow::_error(const QNetworkReply::NetworkError &code)
{
	qDebug() << "erreur" << code;
	QNetworkReply *nReply = qobject_cast<QNetworkReply*>( sender() );
	if( nReply )
	{
		qDebug() << "erreur" <<  nReply->errorString();
	}
}
void    MainWindow::_error(const QList<QSslError> &errors)
{
	qDebug() << "erreurs SSL" << errors;
	foreach (QSslError erreur, errors)
	{
		qDebug() << "erreurs SSL" << erreur.errorString();
	}
}


