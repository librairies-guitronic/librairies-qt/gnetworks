#include "GNetworkAccessManager.h"

#include <QFile>
#include <QDebug>
#include <QHttpMultiPart>
#include <QUrlQuery>
#include <QNetworkProxy>
#include <QAuthenticator>


GNetworkAccessManager::GNetworkAccessManager(QObject *parent)
	: QNetworkAccessManager(parent)
{
	connect(this, &QNetworkAccessManager::finished, this, &GNetworkAccessManager::_networkFinished );
	//pour debug
	/*connect(this, &QNetworkAccessManager::authenticationRequired, this, &GNetworkAccessManager::_authenticationRequired );
	connect(this, &QNetworkAccessManager::preSharedKeyAuthenticationRequired, this, &GNetworkAccessManager::_preSharedKeyAuthenticationRequired );
	connect(this, &QNetworkAccessManager::encrypted, this, &GNetworkAccessManager::_encrypted );
	connect(this, &QNetworkAccessManager::proxyAuthenticationRequired, this, &GNetworkAccessManager::_proxyAuthenticationRequired );
	connect(this, &QNetworkAccessManager::sslErrors, this, &GNetworkAccessManager::_sslErrors );*/
}

/**
 * Envois une requète GET pour recevoir du JSon
 * @brief getJSon
 * @param urlString
 * @param listProps
 */
QNetworkReply *GNetworkAccessManager::getJSon(const QString &urlString, const QList<QPair<QString, QVariant> > &listProps)
{
	QNetworkRequest request = QNetworkRequest( QUrl( urlString ) );
	request.setRawHeader("Accept", "application/json");
	request.setRawHeader("Accept-Charset", "utf-8");
	QNetworkReply *response = get( request );
	_assignProperty(response, listProps);
	_connectResponse(response);
	emit onBusy(true);
	return response;
}

/**
 * Envois une requète DELETE pour recevoir du JSon
 * @brief deleteJSon
 * @param urlString
 * @param listProps
 */
QNetworkReply *GNetworkAccessManager::deleteJSon(const QString &urlString, const QList<QPair<QString, QVariant> > &listProps)
{
	QNetworkRequest request = QNetworkRequest( QUrl( urlString ) );
	request.setRawHeader("Accept", "application/json");
	request.setRawHeader("Accept-Charset", "utf-8");
	QNetworkReply *response = deleteResource( request );
	_assignProperty(response, listProps);
	_connectResponse(response);
	emit onBusy(true);
	return response;
}

/**
 * Envois d'une requète en application/x-www-form-urlencoded
 * @brief postQuery
 * @param urlString
 * @param listeQuery
 * @param listProps
 * @return
 */
QNetworkReply *GNetworkAccessManager::postQuery(const QString &urlString, const QList< STRUCT_POST_QUERY > &listeQuery, const LIST_PROPERTY &listProps)
{
	if( !listeQuery.isEmpty() )
	{
		QUrlQuery postData;
		postData.setQueryItems(listeQuery);
		QNetworkRequest request = QNetworkRequest( QUrl( urlString ) );
        request.setRawHeader("User-Agent", QString("Guitronic %1 %1").arg(TARGET_NAME).toLatin1() ); //.arg(VERSION)
		request.setHeader(QNetworkRequest::ContentTypeHeader,"application/x-www-form-urlencoded");

		//Envois et connexion de la requete
		QNetworkReply *response = post(request, postData.toString(QUrl::FullyEncoded).toLatin1());
		_assignProperty(response, listProps);
		_connectResponse(response);
		emit onBusy(true);
		return response;
	}
	else
	{
		qWarning() << "Liste de requètes vide";
	}
	return Q_NULLPTR;
}

/**
 * Envois de fichier post en multipart/form-data
 * @brief postFile
 * @param urlString
 * @param name
 * @param fileName
 * @param listProps
 * @return
 */
QNetworkReply *GNetworkAccessManager::postFile(const QString &urlString, const QString &name, const QString &fileName, const LIST_PROPERTY &listProps)
{
	if( QFile::exists(fileName) )
	{
		QFile *fileUpload = new QFile(fileName);
		if( fileUpload->open( QIODevice::ReadOnly ) )
		{
			QUrl url = QUrl(urlString);
			QNetworkRequest request = QNetworkRequest( url );
            request.setRawHeader("User-Agent", QString("Guitronic %1 %2").arg(TARGET_NAME).arg(VERSION).toLatin1() );

			QString boundary = "margintest";
			QByteArray aadata = QByteArray( QString("--" + boundary + "\r\n").toLatin1() );
			aadata.append( QString("%1\r\n").arg(url.fileName()).toLatin1() );
			aadata.append( QString("--%1\r\n").arg(boundary).toLatin1() );
			aadata.append( QString("Content-Disposition: form-data; name=\"%1\"; filename=\"%2\"\r\n").arg(name).arg(fileUpload->fileName()).toLatin1() );
			aadata.append("Content-Type: image/jpeg\r\n\r\n");
			aadata.append( fileUpload->readAll() );
			aadata.append("\r\n");
			aadata.append( QString("--%1--\r\n").arg(boundary).toLatin1() );
			request.setHeader(QNetworkRequest::ContentTypeHeader, QString("multipart/form-data; boundary=" + boundary).toLatin1() );
			request.setHeader(QNetworkRequest::ContentLengthHeader, QString::number(aadata.length()).toLatin1() );
			request.setHeader(QNetworkRequest::ContentDispositionHeader, QString("form-data; name=\"%1\"").arg(name).toLatin1() );

			//Envois et connexion de la requete
			QNetworkReply *response = post( request, aadata );
			_assignProperty(response, listProps);
			_connectResponse(response);

			//Libere le fichier
			fileUpload->close();
			delete fileUpload;
			emit onBusy(true);
			return response;
		}
		else
		{
			qWarning() << "Accès en lecture impossible:" << fileName;
		}
		delete fileUpload;
	}
	else
	{
		qWarning() << "Le fichier n'existe pas:" << fileName;
	}
	return Q_NULLPTR;
}

/**
 * Envois plusieurs éléments textes ou fichiers en QHttpMultiPart::FormDataType
 * @brief postMultipart
 * @param urlString
 * @param listeTextes
 * @param listeFichiers
 * @param listProps
 * @return
 */
QNetworkReply *GNetworkAccessManager::postMultipart(const QString &urlString, const QList< STRUCT_POST_TEXTE > &listeTextes, const QList< STRUCT_POST_FILE > &listeFichiers, const LIST_PROPERTY &listProps)
{
	QNetworkRequest request = QNetworkRequest( QUrl(urlString) );
    request.setRawHeader("User-Agent", QString("Guitronic %1 %2").arg(TARGET_NAME).arg(VERSION).toLatin1() );
	QHttpMultiPart *multiPart = new QHttpMultiPart(QHttpMultiPart::FormDataType, this);

	//Construit le contenu
	foreach (const STRUCT_POST_TEXTE champsTexte, listeTextes)
	{
		QHttpPart textPart;
		textPart.setHeader(QNetworkRequest::ContentDispositionHeader, QString("form-data; name=\"%1\"").arg(champsTexte.first).toLatin1() );
		textPart.setBody( champsTexte.second.toLatin1() );
		multiPart->append(textPart);
	}

	foreach (const STRUCT_POST_FILE champsFichier, listeFichiers)
	{
		QFile *fichier = new QFile(champsFichier.second, multiPart);
		if( fichier->exists() && fichier->open(QIODevice::ReadOnly) )
		{
			QHttpPart imagePart;
			imagePart.setHeader(QNetworkRequest::ContentTypeHeader, QVariant("application/octet-stream"));
			imagePart.setHeader(QNetworkRequest::ContentDispositionHeader, QString("form-data; name=\"%1\"; filename=\"%2\"").arg(champsFichier.first).arg(fichier->fileName()) );
			imagePart.setBodyDevice(fichier);
			fichier->setParent(multiPart);
			multiPart->append(imagePart);
		}
		else
		{
			if( !fichier->exists() )
			{
				qWarning() << "Le fichier n'existe pas:" << champsFichier.second;
			}
			else {
				qWarning() << "Accès en lecture impossible:" << champsFichier.second;
			}
			delete fichier;
			return Q_NULLPTR;
		}
	}

	//Envois la requete
	QNetworkReply *response = post(request, multiPart);
	_assignProperty(response, listProps);
	_connectResponse(response);
	multiPart->setParent(response);
	emit onBusy(true);
	return response;
}


/**
 * Envois post terminé
 * @brief _networkFinished
 */
void GNetworkAccessManager::_networkFinished(QNetworkReply *reply)
{
	emit onBusy(false);
	if( reply )
	{
		emit reponseDone(*reply);
		reply->deleteLater();
	}
	else
	{
		qWarning() << "Probleme de reception de la requete Post";
	}
}

/*void GNetworkAccessManager::_authenticationRequired(QNetworkReply *reply, QAuthenticator *)
{
	qDebug() << "_authenticationRequired" << reply->url();
}
void GNetworkAccessManager::_preSharedKeyAuthenticationRequired(QNetworkReply *reply, QSslPreSharedKeyAuthenticator *)
{
	qDebug() << "_preSharedKeyAuthenticationRequired" << reply->url();
}
void GNetworkAccessManager::_encrypted(QNetworkReply *reply)
{
	qDebug() << "_encrypted" << reply->url();
}
void GNetworkAccessManager::_proxyAuthenticationRequired(const QNetworkProxy &proxy, QAuthenticator *)
{
	qDebug() << "_proxyAuthenticationRequired" << proxy.hostName();
}
void GNetworkAccessManager::_sslErrors(QNetworkReply *reply, const QList<QSslError> &errors)
{
	qDebug() << "_sslErrors" << reply->url() << errors;
}*/



/**
 * Assigne les propriétés à la reponse
 * @brief _assignProperty
 * @param response
 * @param listProps
 */
void GNetworkAccessManager::_assignProperty(QNetworkReply *response, const LIST_PROPERTY &listProps)
{
	foreach (const STRUCT_PROPERTY propertie, listProps)
	{
		response->setProperty( propertie.first.toLatin1(), propertie.second);
	}
}

/**
 * Connecte les reponses pour les retours
 * @brief _connectResponse
 * @param response
 */
void GNetworkAccessManager::_connectResponse(QNetworkReply *response)
{
	connect(response, SIGNAL(downloadProgress(qint64,qint64)), this, SIGNAL(replyProgress(qint64,qint64)), Qt::QueuedConnection);
	connect(response, SIGNAL(uploadProgress(qint64,qint64)), this, SIGNAL(replyProgress(qint64,qint64)), Qt::QueuedConnection);
	connect(response, SIGNAL(error(QNetworkReply::NetworkError)), this, SIGNAL(replyError(QNetworkReply::NetworkError)) );
	connect(response, SIGNAL(sslErrors(QList<QSslError>)), this, SIGNAL(replyError(QList<QSslError>)) );
}
